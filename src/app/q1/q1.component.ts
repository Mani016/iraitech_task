import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-q1',
  templateUrl: './q1.component.html',
  styleUrls: ['./q1.component.scss']
})
export class Q1Component implements OnInit {
  series = [2, 3, 10, 15, 26, 35, 50, 63];
  index: number;
  number: number;

  constructor() { }

  ngOnInit(): void {
  }
  getValue() {
    this.number = this.series[this.index]
  }

}
