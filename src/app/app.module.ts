import { BrowserModule } from '@angular/platform-browser';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';

import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Q1Component } from './q1/q1.component';
import { Q2Component } from './q2/q2.component';
import { Q3Component } from './q3/q3.component'; 
import { Q4Component } from './q4/q4.component';
import { C1Component } from './q4/c1/c1.component';
import { C2Component } from './q4/c2/c2.component';
import { C3Component } from './q4/c3/c3.component';
import { C4Component } from './q4/c4/c4.component';
const routes: Routes = [
  /**
   * MAIN
   * 
   */
  {
    path: 'main',
    component: MainComponent
  },
  {
    path: 'home/q1',
    component:Q1Component
  },
  {
    path: 'home/q2',
    component:Q2Component,
  },
  {
    path: 'home/q3',
    component:Q3Component
  },
  {
    path: 'home/q4', 
    component:Q4Component
  },
  {
    path: '',
    redirectTo: 'main',
    pathMatch: 'full',
  },
  {
    path: '**',
    redirectTo: 'main',
    pathMatch: 'full',
  },
]
@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    Q1Component,
    Q2Component,
    Q3Component,
    Q4Component,
    C1Component,
    C2Component,
    C3Component,
    C4Component,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  // schemas: [
  //   CUSTOM_ELEMENTS_SCHEMA,
  //   NO_ERRORS_SCHEMA
  // ]
})
export class AppModule { }
