import { Component, Input, Output, EventEmitter,OnInit } from '@angular/core';

@Component({
  selector: 'app-c1',
  templateUrl: './c1.component.html',
  styleUrls: ['./c1.component.scss']
})
export class C1Component implements OnInit {
  @Input() value: any;
  @Output() valueChange = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

}
