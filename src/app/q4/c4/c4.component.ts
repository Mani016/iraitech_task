import { Component, Input, Output, EventEmitter,OnInit } from '@angular/core';

@Component({
  selector: 'app-c4',
  templateUrl: './c4.component.html',
  styleUrls: ['./c4.component.scss']
})
export class C4Component implements OnInit {
  @Input() value: any;
  @Output() valueChange = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

}
